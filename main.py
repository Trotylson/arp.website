from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from configparser import ConfigParser
from libs.database import ENGINE
from libs.models import Base

from libs.hashing import Hasher
from routers import root, terminal, python3, home, base, user


# origins = [
#     "*"
# ]

# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=origins,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )

Base.metadata.create_all(bind=ENGINE)

config = ConfigParser()
config.read("config/config.ini")

hasher = Hasher()

app = FastAPI(title=".:ARP:.", version="1.0", docs_url="/arp-api", redoc_url="/arp-redoc")
app.mount("/static", StaticFiles(directory="static"), name="static")


app.include_router(root.router)
app.include_router(terminal.router)
app.include_router(python3.router)
app.include_router(home.router)
app.include_router(base.router)
app.include_router(user.router)

