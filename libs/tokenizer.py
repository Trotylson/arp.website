from jose import jwt
from configparser import ConfigParser
from libs.models import User
from datetime import timedelta, timezone, datetime


config = ConfigParser()
config.read("config/config.ini")


class Tokenizer():
    def __init__(self):
        pass

    def check_admin(self, token, db):
        if not token:
            return False
        try:
            scheme,_,param = token.partition(" ")
            payload = jwt.decode(param, config.get("security", "jwt_secret_key"), config.get("security", "algorithm"))
            user = db.query(User).filter(User.name==payload['sub']).first()
            if not user:
                return False
            if user.is_admin is False:
                return False
            return user
        except Exception:
            return False
        
    def check_user(self, token, db):
        if not token:
            print('No token')
            return False
        try:
            scheme,_,param = token.partition(" ")
            payload = jwt.decode(token=param, key=config.get("security", "jwt_secret_key"), algorithms=config.get("security", "algorithm"))
            user = db.query(User).filter(User.name==payload['sub']).first()
            if not user:
                print('no user')
                return False
            if user.is_active is False:
                print('user inactive')
                return False
            return user
        except Exception as e:
            print(e)
            return False

    def extend_token_lifespan(self, username, template):
        data = {
            "sub": username,
            "exp": datetime.now(tz=timezone.utc) + timedelta(minutes=30)
            }
        jwt_token = jwt.encode(
            data, config.get("security", "jwt_secret_key"), algorithm=config.get("security", "algorithm")
            ) # expires variable
        
        template.set_cookie(
            key="access_token", value=f"Bearer {jwt_token}", httponly=True)
        return template
