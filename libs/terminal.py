from libs.models import User

class CLIFormater():
    def __init__(self):
        pass

    def command_sort(self, command):
        return command.split()

    def split_path(self, path):
        return path.split('/')
                
    def format_directory(self, dir):
        try:
            # print(f"formating: {dir}")
            if dir == '/':
                return "linux"
            else:
                if dir[-1] == '/':
                    dir = dir[:-1]
                if dir[0] == '/':
                    dir = dir[1:]
                return dir
        except IndexError:
            return "linux/home"

    def list_files_and_directories(self, location=None):
        import os
        dir_list = []
        my_path = self.my_path
        if location:
            dir = self.format_directory(location)
            dir = f"{my_path}/{dir}"
        else:
            dir = f"{my_path}"

        if dir == 'linux' or dir == '/':
            dir = 'linux'

        # print(self.my_path)
        try:
            for x in os.listdir(f"{dir}"):
                dir_list.append(x)
            return dir_list

        except Exception as e:
            return [str(e)]

    def change_directory(self, command):

        msg = ''

        if len(command) == 1 or command[1] == 'home':
            self.my_path = 'linux/home'
        else:
            path_list = self.split_path(command[1])
            # print(path_list)
            for dir in path_list:
                if dir == '..':
                    actual_dir_list = self.split_path(self.my_path)
                    if len(actual_dir_list) > 1:
                        new_dir = ''
                        actual_dir_list = actual_dir_list[:-1]
                        # print(actual_dir_list)
                        for x in actual_dir_list:
                            new_dir += f'{x}/'
                        print(f"new dir: {new_dir}")
                        self.my_path = new_dir[:-1]
                        print(self.my_path)
                elif dir in list(self.list_files_and_directories()):
                    if dir.endswith('.txt'):
                        msg = [f"arp: cd: {dir}: no such file or directory"]
                        break
                    else:
                        self.my_path += f'/{dir}'
                else:
                    msg = [f"arp: cd: {dir}: no such file or directory"]
                    break
        
        self.data['my_location'] = self.my_path
        self.data['content']['status'] ='success'
        self.data['content']['msg'] = msg

    def show_directory(self):
        pwd = self.split_path(self.my_path)
        directory = ''
        if pwd[0] == 'linux':
            pwd[0] = ""
        
        for dir in pwd:
            directory += f'{dir}/'
        
        # print(directory)
        return directory

    def open_to_read(self, path):
        try:
            with open(f"{path}", 'r') as f:
                content = f.read().split('\n')
                # print(content)
            return content
        except:
            return [f"arp: {path}: no such file or directory"]

    def concatenate_file(self, command):
        try:
            if command[1] in self.list_files_and_directories():
                with open(f"{self.my_path}/{command[1]}", 'r') as f:
                    content = f.read().split('\n')
                    # print(content)
                return content
            else:
                return [f"arp: {command[1]}: no such file or directory"]
        except:
            return [f"arp: cat: usage: cat <file>"]


class Commands(CLIFormater):
    def __init__(self, user_token, db):
        self.user = db.query(User).filter(User.name==user_token.name).first()
        self.db = db
        self.user_token = user_token

        self.my_path = self.user.path
        self.last_path = '/'
        self.username = self.user.name
        self.data = {
            'my_location': self.my_path,
            'last_location': self.last_path,
            'full_path': '',
            'username': self.username,
            'content': {
                'status': 'success',
                'msg': [],
                'buttons': []
            },
        }

    def command_recognition(self, command):
        # print(command)
        # print(self.username)

        self.data['content']['msg'] = ''
    
        location = self.split_path(self.my_path)
        if len(location) > 1:
            self.data['last_location'] = location[-1]
        else:
            self.data['last_location'] = '/'

        if len(command) == 0:
            return self.data
# cd
        if command[0] == 'cd':
            try:
                command[1] = self.format_directory(command[1])
                # print(f"complied: %s" % command[1])
            except:
                pass
            self.change_directory(command)
            # print(self.my_path)
# ls        
        elif command[0] == 'ls':
            try:
                self.data['content']['msg'] = self.list_files_and_directories(f"{command[1]}")
            except:
                self.data['content']['msg'] = self.list_files_and_directories()
# pwd
        elif command[0] == 'pwd':
            self.data['content']['msg'] = [self.show_directory()]
# cat
        elif command[0] == 'cat':
            self.data['content']['msg'] = self.concatenate_file(command)
# clear
        elif command[0] == 'clear':
            self.data['content']['msg'] = []
# help
        elif command[0] == 'help':
            self.data['content']['msg'] = self.open_to_read("textmaterials/help.txt")
        else:
            self.data['content']['msg'] = [f"arp: {command[0]}: command not found"]

        location = self.split_path(self.my_path)
        if len(location) > 1:
            self.data['my_location'] = location[-1]
        
        self.data['content']['buttons'] = self.list_files_and_directories()
        self.data['full_path'] = self.show_directory()

        db_payload = {
            "path": self.my_path,
        }

        user_save = self.db.query(User).filter(User.name==self.user_token.name)
        user_save.update(db_payload)
        self.db.commit()


        return self.data
