$(document).ready(function() {

    var userCard = document.getElementById("userCard");

    // card load
    function do_revelation(_object, time) {
        var i = 0;
        var k = window.setInterval(function() {
            if (i >= 100) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i++;
            }
        }, time);
    };

    do_revelation(userCard, 5);
});