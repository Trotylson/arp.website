$(document).ready(function() 
{

    function write_text(_object, _text, time) {
        var _index = 0;
        var p = window.setInterval(function() {
            _object.innerHTML += _text[_index];
            _index++;
            if (_index == _text.length) {
                clearInterval(p);
            }
        }, time);
    };

    if($("info"))
    {
        var info = document.getElementById("info");
        var info_value = document.getElementById("info_value");
        var response = info_value.getAttribute("value");
        const message = JSON.parse(response);
        console.log(message.status);
        info.style.opacity = 1;

        if(message.status == "ACCESS GRANTED")
        {
            info.style['border-color'] = "chartreuse";
            info.style.color = "chartreuse";
        }

        write_text(info, message['status'], 100)
        setTimeout(
            function() 
            {
                window.location.href = message.url;
            }, 2000
        )}
});