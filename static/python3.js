$(document).ready(function() {

    var python3_card = document.getElementById("python3");

    // python3_card.style.opacity = 0;

    // card load
    function do_revelation(_object, time) {
        var i = 0;
        var k = window.setInterval(function() {
            if (i >= 100) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i++;
            }
        }, time);
    };

    do_revelation(python3_card, 5);
});