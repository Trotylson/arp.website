$(document).ready(function() {

    

    fetch("/terminal",{
        method: "POST",
        body: JSON.stringify({"input": "ls"})})
        .then(response => response.json())
        .then(result => {
            console.log(JSON.stringify(result))
            
            var my_location = result['my_location'];
            var last_location = result['last_location'];
            var full_path = result['full_path'];
            var status = result['content']['status'];
            var msg = result['content']['msg'];
            var buttons = result['content']['buttons'];
            var username = result['username'];

            // set values
            var user = username + "@arp"
            document.getElementById("full_path").innerHTML = full_path;


            // prompt
            document.getElementById("prompt").innerHTML = user + " [ "+ my_location +" ] # ";
        });

// terminal data transfer
    $("#terminal_input").keypress(function(e) {
        if (e.keyCode == 13)
        {
            var _input = e.target.value;
            fetch("/terminal",{
                method: "POST",
                body: JSON.stringify({"input": e.target.value})})
                .then(response => response.json())
                .then(result => {
                    console.log(JSON.stringify(result))
                    
                    var my_location = result['my_location'];
                    var last_location = result['last_location'];
                    var full_path = result['full_path'];
                    var status = result['content']['status'];
                    var msg = result['content']['msg'];
                    var buttons = result['content']['buttons'];
                    var username = result['username'];
        
        
                    // set values
                    var user = username + "@arp"
                    document.getElementById("full_path").innerHTML = full_path;

                    // prompt
                    if(_input == "clear") {
                        document.getElementById("prompt_bf").innerHTML = "";
                    } else {
                        document.getElementById("prompt_bf").style.color = "chartreuse";
                        document.getElementById("prompt_bf").innerHTML += user + " [ "+ last_location +" ] # " + _input + "<br>";
                        for(var i = 0; i < msg.length; i++)
                        {
                            if (msg[i].includes(".txt"))
                            {
                                // document.getElementById("prompt_bf").innerHTML += "-r--r--r-- root root 0 0 " + msg[i]+"<br>";
                                document.getElementById("prompt_bf").innerHTML += msg[i]+"<br>";
                            } else {
                                // document.getElementById("prompt_bf").innerHTML += "dr--r--r-- root root 0 0 " + msg[i]+"<br>";
                                document.getElementById("prompt_bf").innerHTML += msg[i]+"<br>";
                            }
                        }
                        document.getElementById("prompt_bf").innerHTML += "<br>";
                    }
                    document.getElementById("prompt").innerHTML = user + " [ "+ my_location +" ] # ";
                });
            e.target.value = "";
        };
    });


// terminal click
    $("#terminal").click(function() {
        document.getElementById("terminal_input").focus();
    });

// terminal revelation
    function do_revelation(_object, time) {
        var i = 0;
        var k = window.setInterval(function() {
            if (i >= 100) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i++;
            }
        }, time);
    };

    do_revelation(document.getElementById("terminal_area"), 5);

});