$(document).ready(function() {

    console.log("done :)");

    var base = document.getElementById("base");
    
    // python3_card.style.opacity = 0;

    // card load
    function do_revelation(_object, time) {
        var i = 0;
        var k = window.setInterval(function() {
            if (i >= 100) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i++;
            }
        }, time);
    };

    do_revelation(base, 5);

    // card unload
    function fade(_object, time) {
        var i = 100;
        var k = window.setInterval(function() {
            if (i <= 0) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i--;
            }
        }, time);
    };

    $("#terminate_button").click(function() {
        console.log("button clicked");
        console.log(base);
        fade(base, 5);
        setTimeout(function() {
            window.location.href = "/terminate_session";
        }, 100);
    });


    var home_button = document.getElementById("home_button");
    var terminal_button = document.getElementById("terminal_button");
    var python3_button = document.getElementById("python3_button");
    var user_button = document.getElementById("user_button");

    function fader(path)
    {
        fade(base, 5);
        setTimeout(function() {
            window.location.href = path;
        }, 1000);
    }

    $("#home_button").click(function() {
        console.log(home_button.value);
        fader(home_button.value);
    });
    $("#terminal_button").click(function() {
        console.log(terminal_button.value);
        fader(terminal_button.value);
    });
    $("#python3_button").click(function() {
        console.log(python3_button.attributes.value.value);
        fader(python3_button.attributes.value.value)
    });
    $("#user_button").click(function() {
        console.log(user_button.value);
        fader(user_button.value);
    });

    
    function write_text(_object, _text, time) {
        var _index = 0;
        var p = window.setInterval(function() {
            _object.innerHTML += _text[_index];
            _index++;
            if (_index == _text.length) {
                clearInterval(p);
            }
        }, time);
    };

    var msg = "With great power comes great responsibility!";


    write_text(document.getElementById('footer_msg'), msg, 100);
    document.getElementById('footer_msg').innerHTML = '';
    setTimeout(timeout = 1000)
    setInterval(function()
    {
        write_text(document.getElementById('footer_msg'), msg, 100);
        document.getElementById('footer_msg').innerHTML = '';
        
    }, 6000)


    // $("#button_terminate").click(function() {
    //     console.log("button clicked");
    //     fetch("/terminate_session", {
    //         method: "POST"
    //     });
    // });

});