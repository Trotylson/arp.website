$(document).ready(function() 
{

    var welcome_card = document.getElementById("welcome_card");

    // welcome_card.style.opacity = 0;

    // card load
    function do_revelation(_object, time) {
        var i = 0;
        var k = window.setInterval(function() {
            if (i >= 100) {
                clearInterval(k);
            } 
            else {
                _object.style.opacity = i / 100;
                i++;
            }
        }, time);
    };

    do_revelation(welcome_card, 5);


    function write_text(_object, _text, time) {
        var _index = 0;
        var p = window.setInterval(function() {
            _object.innerHTML += _text[_index];
            _index++;
            if (_index == _text.length) {
                clearInterval(p);
            }
        }, time);
    };

});