from fastapi import APIRouter, Request, Depends
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from libs.hashing import Hasher
from sqlalchemy.orm import Session
from libs.database import get_db
import libs.tokenizer as Tokenizer


templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)
hasher = Hasher()
tokenizer = Tokenizer.Tokenizer()


@router.get("/home")
def main(request: Request, db:Session=Depends(get_db)):
    """
    main page
    """        
    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)
    if not user:
        return RedirectResponse("/")

    # with open(f"info/update.txt", 'r') as f:
    #     update = f.read().split('\n')
    # print(update)
    template = templates.TemplateResponse("home.html", {"request": request, "rank": user.rank})
    return tokenizer.extend_token_lifespan(user.name, template)
