from fastapi import APIRouter, Request, Depends
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from libs.hashing import Hasher
from sqlalchemy.orm import Session
from libs.database import get_db
from jose import jwt
from libs.terminal import Commands
import libs.tokenizer as Tokenizer
import json


templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)
hasher = Hasher()
tokenizer = Tokenizer.Tokenizer()


@router.get("/terminal")
def main(request: Request, db:Session=Depends(get_db)):
    """
    main page
    """
    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)
    if not user:
        return RedirectResponse("/")
    template = templates.TemplateResponse("terminal.html", {"request": request})
    return tokenizer.extend_token_lifespan(user.name, template)


@router.post("/terminal")
async def get_data(request: Request, db:Session=Depends(get_db)):
    """
    data endpoint
    """
    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)

    if not user:
        return RedirectResponse("/")

    cli = Commands(user, db)
    
    packet = await request.body()
    packet = json.loads(packet.decode())
    # print(packet)
    _input = packet['input']
    command = cli.command_sort(_input)

    # print(f"sorted command: {command}")

    data = cli.command_recognition(command)

    if data['my_location'] == 'linux':
        data['my_location'] = '/'

    return data
