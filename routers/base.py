from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
# from fastapi.security import OAuth2PasswordBearer
from fastapi import APIRouter, Request, Response
from libs.terminal import CLIFormater, Commands


# oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login/token")
templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)


# @router.get("/base")
# def root(request: Request):
#     """
#     root page
#     """
#     return templates.TemplateResponse("base.html", {"request": request})


