from fastapi import APIRouter, Request, Depends
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from libs.hashing import Hasher
from sqlalchemy.orm import Session
from libs.database import get_db
from jose import jwt
from libs.terminal import Commands
import libs.tokenizer as Tokenizer
import json


templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)
hasher = Hasher()
tokenizer = Tokenizer.Tokenizer()


@router.get("/user")
def main(request: Request, db:Session=Depends(get_db)):
    """
    main page
    """
    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)
    if not user:
        return RedirectResponse("/")
    template = templates.TemplateResponse("user.html", {"request": request, "user": user})  # noqa: E501
    return tokenizer.extend_token_lifespan(user.name, template)


