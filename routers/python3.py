from fastapi import APIRouter, Depends, Request
from fastapi.responses import RedirectResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session

import libs.tokenizer as Tokenizer
from libs.database import get_db
from libs.hashing import Hasher

# oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login/token")
templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)
hasher = Hasher()
tokenizer = Tokenizer.Tokenizer()


@router.get("/python3")
def main(request: Request, db:Session=Depends(get_db)):
    """
    main page
    """
    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)
    if not user:
        return RedirectResponse(url="/")
    template = templates.TemplateResponse("python3.html", {"request": request})
    return tokenizer.extend_token_lifespan(user.name, template)
