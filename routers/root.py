from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi import APIRouter, Request, Depends, status, Response, HTTPException
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from libs.models import User
from libs.hashing import Hasher
from sqlalchemy.orm import Session
from libs.database import get_db
from configparser import ConfigParser
from jose import jwt
import libs.tokenizer as Tokenizer


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login/token")
templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)
tokenizer = Tokenizer.Tokenizer()


config = ConfigParser()
config.read("config/config.ini")

hasher = Hasher()
tokenizer = Tokenizer.Tokenizer()


@router.get("/")
def root(response: Response, request: Request, db: Session=Depends(get_db)):
    """
    root page
    """
    token = request.cookies.get("access_token")
    
    if tokenizer.check_user(token, db):
        return RedirectResponse(url="/home")
    else:
        # print("no token")
        return templates.TemplateResponse("root.html", {"request": request})


@router.post("/")
async def login(response: Response, request: Request, db:Session=Depends(get_db)):
    """
    login form
    """
    data = await request.form()
    username = data.get('login')
    password = data.get('password')

    user = db.query(User).filter(User.name==username).first()

    if user is None:
        print("login or password incorrect!")
        return templates.TemplateResponse("blank.html", {"request": request, "msg": '{"status": "ACCESS DENIED", "url": "/"}'})  # noqa: E501

    if user.is_active is False:
        return templates.TemplateResponse("blank.html", {"request": request, "msg": '{"status": "ACCOUNT BLOCKED", "url": "/"}'})  # noqa: E501

    if hasher.verify_password(password, user.password):
        template = templates.TemplateResponse("blank.html", {"request": request, "msg": '{"status": "ACCESS GRANTED", "url": "/home"}'})  # noqa: E501
        return tokenizer.extend_token_lifespan(username, template)

    else: 
        print("Invalid username or password")
        return templates.TemplateResponse("blank.html", {"request": request, "msg": '{"status": "ACCESS DENIED", "url": "/"}'})  # noqa: E501


@router.post("/token", tags = ['login'])
def retrieve_token_after_authentication(form_data:OAuth2PasswordRequestForm=Depends(), db:Session=Depends(get_db)):  # noqa: E501
    """
    OAuth2PasswordRequestForm schema
    """
    # print(form_data.username)
    # print(form_data.password)
    user = db.query(User).filter(User.name==form_data.username).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid username")
    # if user.is_active == False:
    #     return RedirectResponse("/")
    if not Hasher.verify_password(form_data.password ,user.password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid password")
    data = {"sub":form_data.username}
    jwt_token = jwt.encode(
        data, config.get("security", "jwt_secret_key"), config.get("security", "algorithm"))  # noqa: E501
    return {"access_token":jwt_token, "token_type": "bearer"}


@router.get("/terminate_session")
def terminate_session(response: Response, request: Request, db:Session=Depends(get_db)):
    """
    logout / terminate session
    """

    token = request.cookies.get("access_token")
    user = tokenizer.check_user(token, db)
    print(user)
    
    db_payload = {
        "path": 'linux',
    }

    try:
        user_save = db.query(User).filter(User.name==user.name)
        user_save.update(db_payload)
        db.commit()
    except:
        pass
    finally:
        response = RedirectResponse("/", 302)
        response.delete_cookie(key='access_token')
        return response
